var basicDirectives = angular.module('basicDirectives', [])
    .directive('navBar', [function(){
        return {
            restrict: 'E',
            scope: {
                itemlist: '=navlist'
            },
            replace: true,
            templateUrl:'templates/directives/navbar.html',
            controller: function($scope){
                
            }
        };
    }])
    .directive('navItem',[function(){
        return {
            restrict:'E',
            scope:{
                item: '='
            },
            replace:true,
            templateUrl:'templates/directives/navitem.html',
            controller: ['$scope', '$location', function($scope, $location) {
                $scope.linkClick = function(){
                    $location.url($scope.item.url);
                }  
            }]
        }
    }]);