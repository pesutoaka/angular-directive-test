var postsDirective = angular.module('app.directives.posts',['app.providers.post'])
.config(['postListProvider',function($postListProvider){
    $postListProvider.setApiUrl('/content.json');
}])
    .directive('posts',[function(){
        return {
            restrict: 'E',
            scope: {
                title: '=',
                header: '=',
                footer: '=',
                postsurl: '='
            },
            replace: true,
            templateUrl: 'templates/directives/posts.html',
            controller: ['$scope','postList', function( $scope, $postListProvider) {
                if($scope.postsurl.length) $postListProvider.setApiUrl($scope.postsurl);
                $scope.posts;
                $postListProvider.getPosts()
                    .then(function(list){
                        $scope.posts = list;
                });
            }]
        }
    }])
    .directive('post',[function(){
        return {
            restrict: 'E',
            scope: {
                post: '=content'
            },
            replace:true,
            templateUrl: 'templates/directives/post.html',
            controller: [function(){}]
        }
    }]);