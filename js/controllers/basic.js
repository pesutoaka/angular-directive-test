 var basicControllers = angular.module('basicControllers',[])
    .controller('IndexCtrl',['$scope','$http',function($scope,$http){
        $scope.text = "some text from controller";
        
    }])
    .controller('NavigationCtrl', ['$scope', function($scope){
        $scope.itemlist = [
                {url: '/', name:'home'},
                {url: '/posts', name:'user posts'}
            ];
    }])
    .controller('PostsCtrl', ['$scope',function($scope){
        $scope.posts = {
            title:'lista postów',
            footer: 'stopka postów',
            header: 'nagłówek postów',
            url: '/content.json'
        }
    }])
 ;