var postProvider = angular.module('app.providers.post',[])
    .provider('postList',[function(){
        //privates
        var apiUrl = '';
        return {
            setApiUrl:function(url){
                apiUrl = url;
            },
            $get: ['$http',function($http){
                //privates
                return {
                    getPosts: function(){
                        return $http.get(apiUrl)
                            .then(function(resp){
                                return resp.data;
                            });
                    },
                    setApiUrl: function(url){
                        apiUrl = url;
                    }
                }
            }]
        };
    }]);