var app = angular.module('app',['ngRoute','basicControllers','basicDirectives','app.providers.post','app.directives.posts'])
    .config(['$routeProvider','postListProvider',function($routeProvider, $postListProvider){
        $routeProvider.when('/',{templateUrl:'partials/index.html',controller:'IndexCtrl'})
            .when('/posts', {templateUrl: 'partials/posts.html', controller: 'PostsCtrl'})
            .otherwise({redirectTo:'/'});
        $postListProvider.setApiUrl('/content.json')
    }]);